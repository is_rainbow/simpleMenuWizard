# simpleMenuWizard but customlized
Hide default context menu items in Firefox Quantum 91.0(64-bit)

---
preview

![img](https://github.com/isNijikawa/simpleMenuWizard/blob/master/asset/simple.png)

## Instructions

No instruction, I modify tons of thing you can't let me remember them all.

Just download this project, unzip the zip, drag the folder "simpleMenuWizard" into firefox profile dictionary.

Then enjoy your context menu content go away.
